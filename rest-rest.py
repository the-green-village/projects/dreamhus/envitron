"""Connector between the Envitron Public API and The Green Village data platform.

https://my.envitron.nl/public-api/docs/

- A token is valid for 7 days and should be refreshed.
- Up to 1000 requests per user per hour are allowed.
- /buildings/{id}/electricity/actual/ gives the actual electricity usage in W.
  This method returns the sum from all meters within a building.
- /buildings/{id}/electricity/meterreadings/ gives the meter readings in kWh.
  This method is not supported for the houses at The Green Villages
- There is no endpoint for actual gas consumption,
  we can only say how much gas (in m3) was used over a time period.
- /buildings/<building_id>/sensors/export gives measurements in CSV format
  in 1-minute intervals in W and m3 for electricity and gas, respectively.
"""

import logging
import pathlib
import time
from configparser import ConfigParser
import requests
from requests.exceptions import RequestException
from pprint import pformat

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__file__)

# Read config file.
config = ConfigParser()
config.read(pathlib.Path(__file__).parent.resolve() / 'envitron.cfg')
envitron_url = config.get('Envitron', 'url')
envitron_username = config.get('Envitron', 'username')
envitron_password = config.get('Envitron', 'password')
rest_url = config.get('RestProxy', 'url')
rest_username = config.get('RestProxy', 'username')
rest_password = config.get('RestProxy', 'password')
registry_url = config.get('RestProxy', 'schema_registry_url')

# Time between sensor readouts in seconds
# There are 24 devices in the houses and on a 1-minute basis
# this leads to 1440 requests per hour.
# Maximum 1000 requests per user per hour are allowed.
# Therefore, we choose to query data once per 2 minutes
# and send to Kafka in 2-minute intervals.
# The actual data points will still have 1-minute granularity.
PERIOD = 120

# Get schema from the schema registry.
r = requests.get(registry_url, timeout=5)
logger.info(r.text)
logger.info(f"Kafka Schema Registry response code: {r.status_code}")
# value_schema_id = r.json()['id']
schema = r.json()['schema']


# Get access and refresh tokens from Envitron.
r = requests.post(f"{envitron_url}/token/",
                  json={
                      "username": envitron_username,
                      "password": envitron_password
                  })
logger.info(r.text)
logger.info(f"Envitron /token response code: {r.status_code}")
access_token = r.json()['access']
refresh_token = r.json()['refresh']


def refresh_tokens(access_token, refresh_token):
    """Refresh the access token from Envitron if necessary.

    The access token is valid for 15 minutes and can be refreshed
    using the refresh token.
    """

    r = requests.post(f"{envitron_url}/token/verify/",
                      json={
                          "token": access_token
                      })
    logger.debug(r.text)
    logger.info(f"Envitron /token/verify response code: {r.status_code}")

    if r.status_code != requests.codes.ok:
        r = requests.post(f"{envitron_url}/token/refresh/",
                          json={
                              "refresh": refresh_token
                          })
        logger.debug(r.text)
        logger.info(f"Envitron /token/refresh response code: {r.status_code}")
        access_token = r.json()['access']
        refresh_token = r.json()['refresh']

    return access_token, refresh_token


# Get buildings.
# The endpoint return a list of buildings in the following format:
# {
#   "id": 130,
#   "name": "BGDD Delft Woning 1"
# }
r = requests.get(f"{envitron_url}/buildings",
                 headers={"Authorization": f"Bearer {access_token}"},
                 timeout=5)
logger.debug(r.text)
logger.info(f"Envitron /buildings response code: {r.status_code}")
buildings = r.json()

# Get the list of sensors for each building.
for b in buildings:
    # The endpoint return a list of buildings in the following format:
    # {
    #   "id": "energy_electricity_681",
    #   "name": "Hoofdaansluiting - F1",
    #   "type": "energy",
    #   "subtype": "electricity",
    #   "is_meter_reading": false
    # }
    r = requests.get(f"{envitron_url}/buildings/{b['id']}/sensors",
                     headers={"Authorization": f"Bearer {access_token}"},
                     timeout=5)
    logger.debug(r.text)
    logger.info(f"Envitron /buildings/{b['id']}/sensors response code: {r.status_code}")
    print(pformat(r.json()))
    b['sensors'] = r.json()
    # Keep track of the timestamps of the last messages
    # from each device sent to the platform.
    for s in b['sensors']:
        s['last_timestamp'] = 0


while True:
    time.sleep(PERIOD)

    # Refresh the access token from Envitron if necessary.
    access_token, refresh_token = refresh_tokens(access_token, refresh_token)

    records = []
    for b in buildings:
        for s in b['sensors']:
            # Read out values.
            try:
                # The endpoint returns values from today with 1-minute granularity
                # in the CSV format. For example:
                # time,value
                # 1584572400,106.097
                # 1584572460,99.030
                # 1584572520,66.928
                r = requests.get(f"{envitron_url}/buildings/{b['id']}/sensors/export/?sensor_id={s['id']}",
                                 headers={"Authorization": f"Bearer {access_token}"},
                                 timeout=5)
                logger.info(f"Envitron /buildings/{b['id']}/sensors/export/?sensor_id={s['id']} response code: {r.status_code}")
                readings = r.text

            except RequestException as e:
                logger.error(e)
                continue

            if r.status_code != requests.codes.ok:
                continue

            readings = readings.splitlines()
            readings.pop(0)

            for r in readings:
                c = r.split(',')
                t = int(c[0]) * 1000  # Timestamp in milliseconds
                v = float(c[1])

                # Skip all messages that have been sent already.
                if t <= s['last_timestamp']:
                    continue
                s['last_timestamp'] = t

                if s['subtype'] == "electricity":
                    quantity = "power"
                    unit = "W"
                elif s['subtype'] == "gas":
                    quantity = "volume"
                    unit = "m3"
                elif s['subtype'] == "temperature":
                    quantity = "temperature"
                    unit = "celsius"
                elif s['subtype'] == "humidity":
                    quantity = "humidity"
                    unit = "percent"
                else:
                    quantity = s['subtype']
                    unit = ""
                    logger.warning("Unknown quantity")

                value = {
                    "metadata": {
                        "project_id": "dreamhus",
                        "device_id": f"envitron_{s['id']}",
                        "description": s['name'],
                        "type": {"null": None},
                        "manufacturer": {"null": None},
                        "serial": {"null": None},
                        "placement_timestamp": {"null": None},
                        "location": {"string": f"{b['name']}, {b['id']}"},
                        "latitude": {"null": None},
                        "longitude": {"null": None},
                        "altitude": {"null": None}
                    },
                    "data": {
                        "project_id": "dreamhus",
                        "device_id": f"envitron_{s['id']}",
                        "timestamp": t,
                        "values": [
                            {
                                "name": quantity,
                                "description": quantity,
                                "unit": unit,
                                "type": "FLOAT",
                                "value": {
                                    "float": v
                                }
                            }
                        ]
                    }
                }
                logger.debug(value)
                records.append({"value": value})

    n = len(records)
    if n == 0: continue
    chunk_size = 10
    for i in range(n // chunk_size + 1):
        start = i * chunk_size
        end = (i + 1) * chunk_size

        data = {
            # "value_schema_id": value_schema_id,
            "value_schema": schema,
            "records": records[start:end]
        }

        try:
            headers = {
                "Content-Type": "application/vnd.kafka.avro.v2+json",
                "Accept": "application/vnd.kafka.v2+json"
            }
            r = requests.post(rest_url, json=data, headers=headers,
                              auth=(rest_username, rest_password))
            logger.info(r.text)
            logger.info(f"Kafka REST Proxy response code: {r.status_code}")

        except RequestException as e:
            logger.error(e)
            continue
