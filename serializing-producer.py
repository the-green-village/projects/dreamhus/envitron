"""Connector between the Envitron Public API and The Green Village data platform.

https://my.envitron.nl/public-api/docs/

- A token is valid for 7 days and should be refreshed.
- Up to 1000 requests per user per hour are allowed.
- /buildings/{id}/electricity/actual/ gives the actual electricity usage in W.
  This method returns the sum from all meters within a building.
- /buildings/{id}/electricity/meterreadings/ gives the meter readings in kWh.
  This method is not supported for the houses at The Green Villages
- There is no endpoint for actual gas consumption,
  we can only say how much gas (in m3) was used over a time period.
- /buildings/<building_id>/sensors/export gives measurements in CSV format
  in 1-minute intervals in W and m3 for electricity and gas, respectively.
"""

from confluent_kafka import SerializingProducer
from confluent_kafka.serialization import StringSerializer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer

import json
import time
import logging
import pathlib
import requests
from requests.exceptions import RequestException
from configparser import ConfigParser


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    logger = logging.getLogger(__file__)

    # Read config file.
    config = ConfigParser()
    config.read(pathlib.Path(__file__).parent.resolve() / 'envitron.cfg')
    envitron_url = config.get('Envitron', 'url')
    envitron_username = config.get('Envitron', 'username')
    envitron_password = config.get('Envitron', 'password')
    bootstrap_servers = config.get('Kafka', 'bootstrap_servers')
    api_key = config.get('Kafka', 'api_key')
    api_secret = config.get('Kafka', 'api_secret')
    topic = config.get('Kafka', 'topic')
    schema_registry_url = config.get('Kafka', 'schema_registry_url')
    schema_registry_auth = config.get('Kafka', 'schema_registry_auth')

    conf = {
        'url': schema_registry_url,
        'basic.auth.user.info': schema_registry_auth
    }
    schema_registry_client = SchemaRegistryClient(conf)

    schema_str = '{"type":"record","name":"GreenVillageRecord","namespace":"thegreenvillage","fields":[{"name":"project_id","type":"string","doc":"Globally unique id for the project."},{"name":"application_id","type":"string","doc":"Unique id for the application/use case."},{"name":"device_id","type":"string","doc":"Unique id for the device."},{"name":"timestamp","type":"long","doc":"Timestamp (milliseconds since unix epoch) of the measurement."},{"name":"measurements","type":{"type":"array","items":{"type":"record","name":"measurement","fields":[{"name":"measurement_id","type":"string","doc":"Unique id for the measurement."},{"name":"value","type":["null","boolean","int","long","float","double","string"],"doc":"Measured value."},{"name":"unit","type":["null","string"],"doc":"Unit of the measurement.","default":null},{"name":"measurement_description","type":["null","string"],"doc":"Measurement description.","default":null}]}},"doc":"Array of measurements."},{"name":"project_description","type":["null","string"],"doc":"Project description.","default":null},{"name":"application_description","type":["null","string"],"doc":"Application/use case description.","default":null},{"name":"device_description","type":["null","string"],"doc":"Device description.","default":null},{"name":"device_manufacturer","type":["null","string"],"doc":"Device manufacturer.","default":null},{"name":"device_type","type":["null","string"],"doc":"Device type.","default":null},{"name":"device_serial","type":["null","string"],"doc":"Device serial number.","default":null},{"name":"location_id","type":["null","string"],"doc":"Unique id for the location.","default":null},{"name":"location_description","type":["null","string"],"doc":"Location description.","default":null},{"name":"latitude","type":["null","float"],"doc":"Latitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"longitude","type":["null","float"],"doc":"Longitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"altitude","type":["null","float"],"doc":"Altitude in meters above the mean sea level.","default":null}]}'
    conf = {
        'auto.register.schemas': False
    }
    avro_serializer = AvroSerializer(schema_registry_client, schema_str, conf=conf)

    # Create Producer instance
    producer = SerializingProducer({
        'client.id': 'envitron-producer',
        'bootstrap.servers': bootstrap_servers,
        'sasl.mechanisms': 'PLAIN',
        'security.protocol': 'SASL_SSL',
        'sasl.username': api_key,
        'sasl.password': api_secret,
        'ssl.ca.location': '/etc/ssl/certs/ca-certificates.crt',
        'value.serializer': avro_serializer,
        'acks': '1',
        # If you don’t care about duplicates and ordering:
        'retries': 10000000,
        'delivery.timeout.ms': 2147483647,
        'max.in.flight.requests.per.connection': 5
#        # If you don’t care about duplicates but care about ordering:
#        'retries': 10000000,
#        'delivery.timeout.ms': 2147483647,
#        'max.in.flight.requests.per.connection': 1
#        # If you care about duplicates and ordering:
#        'retries': 10000000,
#        'delivery.timeout.ms': 2147483647,
#        'enable.idempotence': True
    })

    delivered_records = 0

    # Optional per-message on_delivery handler (triggered by poll() or flush())
    # when a message has been successfully delivered or
    # permanently failed delivery (after retries).
    def acked(err, msg):
        global delivered_records
        """Delivery report handler called on
        successful or failed delivery of message
        """
        if err is not None:
            logger.error("Failed to deliver message: {}".format(err))
        else:
            delivered_records += 1
            logger.info("Produced record to topic {} partition [{}] @ offset {}"
                  .format(msg.topic(), msg.partition(), msg.offset()))


    try:
        token_file = pathlib.Path(__file__).parent.resolve() / 'token.json'
        with open(token_file, 'r') as f:
            token = json.load(f)
        access_token = token['access']
        refresh_token = token['refresh']
    except:
        access_token = ""
        refresh_token = ""

    # Refresh the access token from Envitron if necessary.
    # The access token is valid for 15 minutes and can be refreshed using the refresh token.
    r = requests.post(f"{envitron_url}/token/verify/",
                      json={
                          "token": access_token
                      })
    logger.debug(r.text)
    logger.info(f"Envitron /token/verify response code: {r.status_code}")

    if r.status_code != requests.codes.ok:
        r = requests.post(f"{envitron_url}/token/refresh/",
                          json={
                              "refresh": refresh_token
                          })
        logger.debug(r.text)
        logger.info(f"Envitron /token/refresh response code: {r.status_code}")

        if r.status_code == requests.codes.ok:
            token = r.json()
        else:
            r = requests.post(f"{envitron_url}/token/",
                              json={
                                  "username": envitron_username,
                                  "password": envitron_password
                              })
            logger.info(r.text)
            logger.info(f"Envitron /token response code: {r.status_code}")

            token = r.json()

        with open(token_file, 'w') as f:
            json.dump(token, f)
        access_token = token['access']


    # Get buildings.
    # The endpoint return a list of buildings in the following format:
    # {
    #   "id": 130,
    #   "name": "BGDD Delft Woning 1"
    # }
    r = requests.get(f"{envitron_url}/buildings",
                     headers={"Authorization": f"Bearer {access_token}"},
                     timeout=5)
    logger.debug(r.text)
    logger.info(f"Envitron /buildings response code: {r.status_code}")
    buildings = r.json()

    # Get the list of sensors for each building.
    for b in buildings:
        # The endpoint return a list of buildings in the following format:
        # {
        #   "id": "energy_electricity_681",
        #   "name": "Hoofdaansluiting - F1",
        #   "type": "energy",
        #   "subtype": "electricity",
        #   "is_meter_reading": false
        # }
        r = requests.get(f"{envitron_url}/buildings/{b['id']}/sensors",
                         headers={"Authorization": f"Bearer {access_token}"},
                         timeout=5)
        logger.info(f"Envitron /buildings/{b['id']}/sensors response code: {r.status_code}")
        logger.info(json.dumps(r.json(), sort_keys=True, indent=4))
        b['sensors'] = r.json()


    for b in buildings:
        for s in b['sensors']:
            # Read out values.
            try:
                # The endpoint returns values from today with 1-minute granularity
                # in the CSV format. For example:
                # time,value
                # 1584572400,106.097
                # 1584572460,99.030
                # 1584572520,66.928
                r = requests.get(f"{envitron_url}/buildings/{b['id']}/sensors/export/?sensor_id={s['id']}",
                                 headers={"Authorization": f"Bearer {access_token}"},
                                 timeout=5)
                logger.info(f"Envitron /buildings/{b['id']}/sensors/export/?sensor_id={s['id']} response code: {r.status_code}")
                readings = r.text
                logger.debug(readings)

            except RequestException as e:
                logger.error(e)
                continue

            if r.status_code != requests.codes.ok:
                continue

            # Process the last message. 
            readings = readings.splitlines()
            if len(readings) == 1:
                continue
            c = readings[-1].split(',')
            t = int(c[0]) * 1000  # timestamp in milliseconds
            v = float(c[1])

            measurement_id = s['subtype']
            unit = None
            # FIXME
            # if s['subtype'] == "electricity":
            #     measurement_id = "power"
            #     unit = "W"
            # if s['subtype'] == "gas":
            #     measurement_id = "volume"
            #     unit = "m3"
            # if s['subtype'] == "temperature":
            #     unit = "celsius"
            # if s['subtype'] == "humidity":
            #     unit = "percent"
            # if s['subtype'] == "water":
            #     unit =

            value = {
                "project_id": "dreamhus",
                "application_id": "envitron",
                "device_id": s['id'],
                "timestamp": t,
                "measurements": [
                    {
                        "measurement_id": measurement_id,
                        "unit": unit,
                        "value": v
                    }
                ],
                "location_id": str(b['id']),
                "location_description": b['name']
            }

            producer.produce(topic=topic, value=value, on_delivery=acked)
            producer.poll(0)

    producer.flush()

    logger.info("{} messages were produced to topic {}!".format(delivered_records, topic))
