# Envitron

https://my.envitron.nl/public-api/docs/

- Up to 1000 requests per user per hour are allowed.
- `/buildings/{id}/electricity/actual/` gives the actual electricity usage in W.
  This method returns the sum from all meters within a building.
- `/buildings/{id}/electricity/meterreadings/` gives the meter readings in kWh.
  This method is not supported for the houses at The Green Villages
- There is no endpoint for actual gas consumption,
  we can only say how much gas (in m3) was used over a time period.
- `/buildings/<building_id>/sensors/export` gives measurements in CSV format
  in 1-minute intervals in W and m3 for electricity and gas, respectively.

Run as a cronjob from the Virtual Machine of The Green Village.

```

crontab -e
* * * * * /usr/bin/python3 /home/jamvanderweijd/dreamhus/envitron/serializing-producer.py 1>/home/pi/dreamhus/envitron/serializing-producer.log 2>&1 &
```
