FROM python:3.6.5-alpine3.7
  
ADD requirements.txt /

RUN pip install --upgrade pip && \
    pip install -r requirements.txt

ADD rest-rest.py /
# ADD envitron.cfg /

CMD [ "python", "rest-rest.py" ]
